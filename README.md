# Appscore

## Getting Started

### Installing

Cocoapods is needed to setup the project as it was the dependency manager of choice. Please follow the installation instruction here https://cocoapods.org

Once the installation of cocoapods is done. A simple `pod install` within the project's root folder will update the workspace and you should be good to go.

## Tests

There are a few unit tests implemented using XCTest and they were created per framework. To run them, open the workspace and press the key combination Cmd + U or Product -> Test.

## Coding Style

A linter was added as one of the pods to enforce a code style throughout the application. The style and conventions are loosely based on GitHub's Swift Style Guidelines.

## Frameworks

* The `EQRequests` framework exposes the bare minimum API to the application in order to fetch the latest earthquakes.
* The `EQList` framework creates and populate the list widget with earthquakes information for the main view controller to place within the application.
* The `EQMap` framework creates and populate the map widget with the passing information for the main view controller to place within the application.

## Improvements

* More in deep unit tests with mocking of objects to guarantee the working of internal on edge cases. e.g. flaky connectivity, data generation failure, etc.
* A Base framework for a more clear dependency between frameworks.
* Loading screens for initial fetching to improve UX

## Author

* **Juan Docal** - (https://www.linkedin.com/in/juandocal)

