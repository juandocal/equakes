//
//  EQRequestsAPI.swift
//  EQRequests
//
//  Created by Juan Carlos Docal Yanez on 7/12/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import Foundation
import UIKit

/// Framework defining the Earthquake Provider's requests API.

/// Protocol defining the entry point to the `EQRequests` Framework.
///
/// - Usage: `let earthquakesProvider: EQRequestsProviderAPI = EQProviderFactory.create(DispatchQueue.global())`.
public protocol EQRequestsProviderAPI: class {

    /// Informs the object conforming to `EQRequestsProviderAPI` about the fetch request.
    ///
    /// - Parameter completion: callback being invoked on the main thread with an optional list of image views
    ///                         and an optional error.
    func fetchEarquakes(completion: @escaping ([EQLocation]?, Error?) -> Void)
}

/// Protocol defining the location details of the earthquakes requested.
public protocol EQLocation {

    var name: String { get }
    var date: String { get }
    var latitude: Double { get }
    var longitude: Double { get }
}

/// Public factory of objects returning internal instances conforming to different access of the API.
public class EQProviderFactory {

    /// Creates an object conforming to the entry protocol `EQRequestsProviderAPI`
    ///
    /// - Parameter queue: queue on which the request will be performed. Main queue if undefined.
    ///
    /// - Returns: The created object of type `EQRequestsProviderAPI`.
    public class func create(_ queue: DispatchQueue = DispatchQueue.main) -> EQRequestsProviderAPI {
        return EQRequest(queue)
    }
}
