//
//  EQRequest.swift
//  EQRequests
//
//  Created by Juan Carlos Docal Yanez on 7/12/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import Foundation

internal class EQRequest {

    internal let queue: DispatchQueue

    internal init(_ queue: DispatchQueue) {
        self.queue = queue
    }
}
