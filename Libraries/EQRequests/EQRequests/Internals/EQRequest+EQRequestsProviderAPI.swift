//
//  EQRequest+EQRequestsProviderAPI.swift
//  EQRequests
//
//  Created by Juan Carlos Docal Yanez on 7/12/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SnapKit

private enum RequestEndpoint {
    static let endpoint: String = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojson"
}

private struct Location: EQLocation {
    var name: String
    var date: String
    var latitude: Double
    var longitude: Double

    internal init(name: String, date: String, latitude: Double, longitude: Double) {
        self.name = name
        self.date = date
        self.latitude = latitude
        self.longitude = longitude
    }
}

extension EQRequest: EQRequestsProviderAPI {

    public func fetchEarquakes(completion: @escaping ([EQLocation]?, Error?) -> Void) {
        print("\(type(of: self)): \(#function)")
        queue.async {
            Alamofire.request(RequestEndpoint.endpoint).responseJSON(queue: self.queue,
                                                                     completionHandler: {[weak self] (response) in
                print("\(type(of: self)): \(#function)")

                guard self != nil else {
                    print("\(#function) Error. Self released!")
                    return
                }

                guard response.result.error == nil else {
                    let description: String = "Error calling GET on \(RequestEndpoint.endpoint)"
                    let error = NSError(domain: "Request", code: 0, userInfo: [ NSLocalizedDescriptionKey: description])
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                    return
                }

                guard let json = response.result.value else {
                    let description: String = "Error getting result on \(RequestEndpoint.endpoint)"
                    let error = NSError(domain: "Request", code: 0, userInfo: [ NSLocalizedDescriptionKey: description])
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                    return
                }

                let swiftyJson = JSON(json)
                var fetchResult: [EQLocation] = []
                for earthquake in swiftyJson["features"] {
                    let name = earthquake.1["properties"]["place"].stringValue
                    guard name != "" else {
                        print("\(type(of: self)): \(#function) Invalid name. Skipping.")
                        continue
                    }

                    let time = earthquake.1["properties"]["time"].doubleValue
                    guard time > 0 else {
                        print("\(type(of: self)): \(#function) Invalid time. Skipping.")
                        continue
                    }

                    // Format date and convert to local
                    let dateTimeStamp = Date(timeIntervalSince1970: time / 1000)
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = NSTimeZone.local
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
                    dateFormatter.dateStyle = DateFormatter.Style.full
                    dateFormatter.timeStyle = DateFormatter.Style.short
                    let date = dateFormatter.string(from: dateTimeStamp)
                    guard date != "" else {
                        print("\(type(of: self)): \(#function) Invalid date. Skipping.")
                        continue
                    }

                    let latitude = earthquake.1["geometry"]["coordinates"][1].doubleValue
                    let longitude = earthquake.1["geometry"]["coordinates"][0].doubleValue
                    let location = Location(name: name, date: date, latitude: latitude, longitude: longitude)
                    fetchResult.append(location)
                }

                DispatchQueue.main.async {
                    completion(fetchResult, nil)
                }
            })
        }
    }
}
