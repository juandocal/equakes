//
//  EQRequestsTests.swift
//  EQRequestsTests
//
//  Created by Juan Carlos Docal Yanez on 7/12/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import XCTest
@testable import EQRequests

public class EQRequestsTests: XCTestCase {

    public func testMainThreadResult() {
        let expectation = XCTestExpectation(description: "It present fetching results on main queue")
        let requestsProvider: EQRequestsProviderAPI = EQProviderFactory.create(DispatchQueue.global())
        requestsProvider.fetchEarquakes { (_, _) in
            XCTAssertTrue(Thread.isMainThread)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5.0)
    }
}
