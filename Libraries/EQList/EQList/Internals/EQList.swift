//
//  EQList.swift
//  EQList
//
//  Created by Juan Carlos Docal Yanez on 7/12/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import Foundation
import UIKit
import EQRequests

internal class EQList: NSObject, UITableViewDelegate, UITableViewDataSource {

    internal var tableView: UITableView = UITableView()

    weak private var requestsProvider: EQRequestsProviderAPI?
    weak private var delegate: EQListDelegate?

    private var locations: [EQLocation] = []
    private let refreshControl: UIRefreshControl = UIRefreshControl()

    internal init(delegate: EQListDelegate, requestsProvider: EQRequestsProviderAPI) {
        self.delegate = delegate
        self.requestsProvider = requestsProvider
        super.init()
        prepareTableView()
        fetchEarthquakes()
    }

    // MARK: - Private

    private func prepareTableView() {
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "EQCell")
    }

    @objc private func refreshData(_ sender: Any) {
        fetchEarthquakes()
    }

    private func fetchEarthquakes() {
        print("\(type(of: self)): \(#function)")

        requestsProvider?.fetchEarquakes(completion: {[weak self] (locations, error) in
            print("\(type(of: self)): \(#function)")

            guard let `self` = self else {
                print("\(#function) Error. Self released!")
                return
            }

            self.refreshControl.endRefreshing()
            guard error == nil else {
                print("\(type(of: self)): \(#function) Error getting locations \(String(describing: error))")
                return
            }

            guard locations != nil else {
                print("\(type(of: self)): \(#function) Error unwrapping locations result \(String(describing: error))")
                return
            }

            if let locations: [EQLocation] = locations {
                self.locations = locations
                self.tableView.reloadData()
            }
        })
    }

    // MARK: - UITableViewDataSource

    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }

    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EQCell", for: indexPath)
        let result: EQLocation = locations[indexPath.row]
        cell.textLabel?.text = result.name
        return cell
    }

    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("\(type(of: self)): \(#function)")
        self.delegate?.cellTapped(location: locations[indexPath.row])
    }
}
