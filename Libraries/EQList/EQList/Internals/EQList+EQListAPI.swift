//
//  EQList+EQListAPI.swift
//  EQList
//
//  Created by Juan Carlos Docal Yanez on 7/12/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import Foundation
import UIKit

extension EQList: EQListAPI {

    public func view() -> UIView {
        return tableView
    }
}
