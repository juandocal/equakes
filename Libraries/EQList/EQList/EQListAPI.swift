//
//  EQListAPI.swift
//  EQList
//
//  Created by Juan Carlos Docal Yanez on 7/12/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import Foundation
import UIKit
import EQRequests

/// Protocol defining the entry point to the `EQList` Framework.
///
/// - Usage: `let listAPI: EQListAPI = EQListFactory.create()`.
public protocol EQListAPI {

    /// Provides the view object containing the UI
    ///
    /// - Returns: The view containing the UI.
    func view() -> UIView
}

/// Protocol defining the cell tapping delegate.
public protocol EQListDelegate: class {

    /// Login UI delegate informing the object conforming to `LoginDelegate` about the login request.
    ///
    /// - Parameter location: Location information for the tapped cell.
    func cellTapped(location: EQLocation)
}

/// Public factory of objects returning the list view.
public class EQListFactory {

    /// Creates a List widget responsible of listing the latest earthquakes.
    ///
    /// - Returns: The created object of type `EQListAPI`.
    public class func create(delegate: EQListDelegate, requestsProvider: EQRequestsProviderAPI) -> EQListAPI {
        return EQList(delegate: delegate, requestsProvider: requestsProvider)
    }
}
