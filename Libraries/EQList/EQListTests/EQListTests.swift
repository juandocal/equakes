//
//  EQListTests.swift
//  EQListTests
//
//  Created by Juan Carlos Docal Yanez on 7/12/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import XCTest
import EQRequests
@testable import EQList

public class EQListTests: XCTestCase, EQRequestsProviderAPI, EQListDelegate {

    private var fetchedEarthquakes: Bool = false

    public func cellTapped(location: EQLocation) { }

    public func fetchEarquakes(completion: @escaping ([EQLocation]?, Error?) -> Void) {
        fetchedEarthquakes = true
        completion(nil, nil)
    }

    public override func setUp() {
        fetchedEarthquakes = false
    }

    public func testListInitialFetchRequest() {
        let listProvider: EQListAPI = EQListFactory.create(delegate: self, requestsProvider: self)
        XCTAssertTrue(fetchedEarthquakes)
    }
}
