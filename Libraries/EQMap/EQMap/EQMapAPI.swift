//
//  EQMapAPI.swift
//  EQMap
//
//  Created by Juan Carlos Docal Yanez on 7/12/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import Foundation
import UIKit
import EQRequests

/// Framework defining the Earthquake Map plotting.

/// Protocol defining the entry point to the `EQMap` Framework.
///
/// - Usage: `let mapProvider: EQMapProviderAPI = EQMapFactory.create()`.
public protocol EQMapProviderAPI: class {

    /// Informs the object conforming to `EQMapProviderAPI` about the fetch request.
    ///
    /// - Parameter location: Location of the marker
    ///
    /// - Returns: The relevant view controller to be placed in the view hierarchy.
    func mapViewController(location: EQLocation) -> UIViewController

    /// Informs the object conforming to `EQMapProviderAPI` about the fetch request.
    ///
    /// - Returns: The relevant view controller to be placed in the view hierarchy.
    func mapViewController() -> UIViewController
}

/// Public factory of objects returning internal instances conforming to different access of the API.
public class EQMapFactory {

    /// Creates an object conforming to the entry protocol `EQMapProviderAPI`
    ///
    /// - Returns: The created object of type `EQMapProviderAPI`.
    public class func create(requestsProvider: EQRequestsProviderAPI) -> EQMapProviderAPI {
        return EQMap(requestsProvider: requestsProvider)
    }
}
