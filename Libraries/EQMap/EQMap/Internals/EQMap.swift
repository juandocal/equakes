//
//  EQMap.swift
//  EQMap
//
//  Created by Juan Carlos Docal Yanez on 8/12/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import Foundation
import GoogleMaps
import EQRequests

internal enum EQMapConfiguration {
    internal static let apiKey: String = "AIzaSyCOpkOZDWt6rp59z6ZVHnAWvebM6FpMfZg"
    internal static let zoom: Float = 6.0
}

internal class EQMap {

    internal lazy var viewController = UIViewController(nibName: nil, bundle: nil)
    weak internal var requestsProvider: EQRequestsProviderAPI?

    internal init(requestsProvider: EQRequestsProviderAPI) {
        self.requestsProvider = requestsProvider
        viewController.view.backgroundColor = UIColor.lightGray
        GMSServices.provideAPIKey(EQMapConfiguration.apiKey)
    }
}
