//
//  EQMap+EQMapProviderAPI.swift
//  EQMap
//
//  Created by Juan Carlos Docal Yanez on 8/12/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import Foundation
import UIKit
import EQRequests
import GoogleMaps

extension EQMap: EQMapProviderAPI {
    public func mapViewController(location: EQLocation) -> UIViewController {
        print("\(type(of: self)): \(#function)")

        /// Prepare Camera
        let camera = GMSCameraPosition.camera(withLatitude: location.latitude,
                                              longitude: location.longitude,
                                              zoom: EQMapConfiguration.zoom)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        self.viewController.view.addSubview(mapView)
        mapView.snp.makeConstraints({ (make) in
            make.edges.equalTo(self.viewController.view)
        })

        /// Prepare Marker
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
        marker.title = location.name
        marker.snippet = location.date
        marker.map = mapView

        return viewController
    }

    public func mapViewController() -> UIViewController {
        print("\(type(of: self)): \(#function)")

        requestsProvider?.fetchEarquakes(completion: {[weak self] (locations, error) in
            print("\(type(of: self)): \(#function)")

            guard let `self` = self else {
                print("\(#function) Error. Self released!")
                return
            }

            guard error == nil else {
                print("\(type(of: self)): \(#function) Error getting locations \(String(describing: error))")
                return
            }

            guard let locations: [EQLocation] = locations else {
                print("\(type(of: self)): \(#function) Error unwrapping locations result \(String(describing: error))")
                return
            }

            guard let defaultCameraLocation: EQLocation = locations.first else {
                print("\(type(of: self)): \(#function) Empty list of locations")
                return
            }

            /// Prepare Camera
            let camera = GMSCameraPosition.camera(withLatitude: defaultCameraLocation.latitude,
                                                  longitude: defaultCameraLocation.longitude,
                                                  zoom: EQMapConfiguration.zoom)
            let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
            self.viewController.view.addSubview(mapView)
            mapView.snp.makeConstraints({ (make) in
                make.edges.equalTo(self.viewController.view)
            })

            for location: EQLocation in locations {

                /// Prepare Markers
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
                marker.title = location.name
                marker.snippet = location.date
                marker.map = mapView
            }
        })

        return viewController
    }
}
