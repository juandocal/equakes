//
//  EQMapTests.swift
//  EQMapTests
//
//  Created by Juan Carlos Docal Yanez on 7/12/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import XCTest
import EQRequests
@testable import EQMap

public class EQMapTests: XCTestCase, EQRequestsProviderAPI {

    private var fetchedEarthquakes: Bool = false

    public func fetchEarquakes(completion: @escaping ([EQLocation]?, Error?) -> Void) {
        fetchedEarthquakes = true
        completion(nil, nil)
    }

    public override func setUp() {
        fetchedEarthquakes = false
    }

    public func testMapInitialFetchRequest() {
        let mapProvider: EQMapProviderAPI = EQMapFactory.create(requestsProvider: self)
        mapProvider.mapViewController()
        XCTAssertTrue(fetchedEarthquakes)
    }
}
