//
//  AppDelegate.swift
//  appscore
//
//  Created by Juan Carlos Docal Yanez on 7/12/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import UIKit

@UIApplicationMain
public class AppDelegate: UIResponder, UIApplicationDelegate {

    public var window: UIWindow?

    public func application(_ application: UIApplication,
                            didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        print("\(type(of: self)): \(#function)")

        self.window = UIWindow(frame: UIScreen.main.bounds)
        let navigation = UINavigationController()
        let mainViewController: ViewController = ViewController()
        navigation.viewControllers = [mainViewController]
        self.window?.rootViewController = navigation
        self.window?.backgroundColor = UIColor.white
        self.window?.makeKeyAndVisible()
        return true
    }

    public func applicationWillResignActive(_ application: UIApplication) {
        print("\(type(of: self)): \(#function)")
    }

    public func applicationDidEnterBackground(_ application: UIApplication) {
        print("\(type(of: self)): \(#function)")
    }

    public func applicationWillEnterForeground(_ application: UIApplication) {
        print("\(type(of: self)): \(#function)")
    }

    public func applicationDidBecomeActive(_ application: UIApplication) {
        print("\(type(of: self)): \(#function)")
    }

    public func applicationWillTerminate(_ application: UIApplication) {
        print("\(type(of: self)): \(#function)")
    }
}
