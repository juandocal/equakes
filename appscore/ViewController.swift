//
//  ViewController.swift
//  appscore
//
//  Created by Juan Carlos Docal Yanez on 7/12/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import UIKit
import EQRequests
import EQList
import EQMap

public class ViewController: UIViewController {

    private var earthquakesProvider: EQRequestsProviderAPI
    private var listProvider: EQListAPI?
    internal var mapProvider: EQMapProviderAPI

    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        earthquakesProvider = EQProviderFactory.create(DispatchQueue.global())
        mapProvider = EQMapFactory.create(requestsProvider: earthquakesProvider)
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        listProvider = EQListFactory.create(delegate: self, requestsProvider: earthquakesProvider)
    }

    internal required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Map",
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(mapTapped))
        prepareList()
    }

    // MARK: - Private

    @objc private func mapTapped() {
        print("\(type(of: self)): \(#function)")
        navigationController?.pushViewController(mapProvider.mapViewController(), animated: true)
    }

    private func prepareList() {

        guard let listProvider: EQListAPI = listProvider else {
            print("\(type(of: self)): \(#function) List provider released")
            return
        }

        view.addSubview(listProvider.view())
        listProvider.view().snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
    }
}
