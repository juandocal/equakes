//
//  ViewController+EQListDelegate.swift
//  appscore
//
//  Created by Juan Carlos Docal Yanez on 8/12/18.
//  Copyright © 2018 Juan Carlos Docal Yanez. All rights reserved.
//

import Foundation
import EQList
import EQRequests

extension ViewController: EQListDelegate {

    public func cellTapped(location: EQLocation) {
        print("\(type(of: self)): \(#function)")
        navigationController?.pushViewController(mapProvider.mapViewController(location: location), animated: true)
    }
}
